## Changelog

### 1.0

#### 1.0.0 [2019-12-28]

* Version 1.0.0
* Adding readme
* Adding basic logic and output
* Adding german translation of timeago
* Adding more language
* Adding satis installation path to config
* Adding some index.html files
* Added languages
* Initial Commit (barebone)