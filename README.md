# PMJ Satis Plugin

PMJ Satis Plugin is a very simple Joomla plugin to display Composer Repositories from a local [Satis](https://github.com/composer/satis) installation.  
I did this because the original Satis output creates a static Html page which can't be styled because it gets overwritten everytime you rebuild the repository.

It has been developed and tested using Joomla 3.8, so this will officially be considered the minimum version although it should work with all Joomla 3+ versions.

## Table of contents

* [Downloads](#downloads)
* [Demo](#demo)
* [How to use it](#how-to-use-it)
* [3rd Party](#3rd-party)

## Downloads

**[Current Stable Version (1.0.0)](https://gitlab.com/pmj-joomla/pmj-satis-plugin/-/archive/1.0.0/pmj-satis-plugin-1.0.0.zip)**

**[Developement Version](https://gitlab.com/pmj-joomla/pmj-satis-plugin/-/archive/dev/pmj-satis-plugin-dev.zip)**

## Demo

ToDo

## How to use it

Simply put this string where you want the repository to display:

`{pmjsatis}`

**Note**: If oyu want to put the output directory of Satis outside your webroot (like I do), make sure you can still access the packages.json through the url you specified in your satis.json and your repository definition in your composer.json or your packages won't be found when using composer!  
You can do this by either copy the file to a place where it can be accessed using http or create a symlink to the file.

## 3rd Party

I used the original [Timeago JQuery Plugin by Ryan McGeary](https://timeago.yarp.com/)