<?php
// no direct access
defined( '_JEXEC' ) or die;

class plgContentPmjsatis extends JPlugin
{
	/**
	* Load the language file on instantiation. Note this is only available in Joomla 3.1 and higher.
	* If you want to support 3.0 series you must override the constructor
	*
	* @var    boolean
	* @since  3.1
	*/
	protected $autoloadLanguage = true;

	/**
  * Plugin method with the same name as the event will be called automatically.
  */
  function onContentPrepare($context, &$article, &$params, $page = 0)
  {
    /*
    * Plugin code goes here.
    * You can access database and application objects and parameters via $this->db,
    * $this->app and $this->params respectively
    */
    $canProceed = $context !== 'com_finder.indexer';

		if (!$canProceed)
		{
			return;
		}
    
    // check formating
    $regex  = '/\{pmjsatis(.*?)\}/';
    if (preg_match_all($regex,$article->text,$matches))
    {
      $doc = JFactory::getDocument();
      $language = JFactory::getLanguage();
      $strftime = '%A, %d-%b-%Y %H:%M:%S %Z';
      $jdate  = 'l, d-M-Y H:i:s e';
      //$doc->addScript('/plugins/content/pmjsatis/js/jquery.timeago.js');
      
      // path to satis.json
      $satis = $this->params->get('satis','');
      // path to satis build directory
      $output  = $this->params->get('output','');
      // path to satis source
      $source  = $this->params->get('source','');
      // get satis version
      $version  = "/VERSION\s=\s'([^;]*)';/";
      preg_match($version,file_get_contents($source.'src/Satis.php'),$version_match);
      $version  = $version_match[1];
      // get time
      $time = filemtime($output.'packages.json');
      $time_iso = date('c',$time);
      $time = date('Y-m-d H:i:s e',$time);
      $time = new JDate($time);
      // get all listed packages
      $repository = json_decode(file_get_contents($output.'packages.json'), true);
      $config = json_decode(file_get_contents($satis.'satis.json'), true);
      
      $packages = $repository['packages'];
      $includes = $repository['includes'];

      $internal = array();
      foreach ($packages as $path => $package)
      {
        
      }

      $external = array();
      foreach ($includes as $path => $include)
      {
        $include  = json_decode(file_get_contents($output.$path), true);
        foreach ($include['packages'] as $name => $package)
        {
          //$external[$name] = $package;
          uasort($package, function ($item1, $item2)
          {
            //return strtotime($item2['version_normalized']) <=> strtotime($item1['version_normalized']);
            return version_compare($item2['version_normalized'],$item1['version_normalized']);
          });
          //uksort($package, 'version_compare');
          //$package  = array_reverse($package);
          /*if (array_key_exists('dev-master',$package))
          {
            $value = $package['dev-master'];
            unset($package['dev-master']);
            $package  = array('dev-master' => $value) + $package;
          }*/
          $authors  = array();
          $releasetime  = 0;
          foreach ($package as $release => $pack)
          {
            // authors
            $authors  = array_merge($authors,$pack['authors']);
            $packtime = strtotime($pack['time']);
            if (!$releasetime || $releasetime < $packtime)
            {
              $releasetime  = $packtime;
              // version
              $external[$name]['version']  = $pack['version'];
              // description
              $external[$name]['description']  = $pack['description'];
              // keywords
              $external[$name]['keywords']  = $pack['keywords'];
              // homepage
              $external[$name]['homepage']  = $pack['homepage'];
              // license
              $external[$name]['license']  = $pack['license'];
              // times
              $external[$name]['time_iso']  = date('c',$packtime);
              $packtime = date('Y-m-d H:i:s e',$packtime);
              $packtime  = new JDate($packtime);
              $external[$name]['time']  = $packtime->format($jdate,true);
            }
            $authors  = array_unique($authors);
            $authors  = array_filter($authors);
            $external[$name]['authors'] = $authors;
            // dist
            if ($pack['dist'])
            {
              $external[$name]['releases'][$release]['dist']  = $pack['dist'];
            }
            // source
            if ($pack['source'])
            {
              $external[$name]['releases'][$release]['source']  = $pack['source'];
            }
            $external[$name]['releases'][$release]['time_iso']  = date('c',strtotime($pack['time']));
            $packtime = date('Y-m-d H:i:s e',strtotime($pack['time']));
            $packtime  = new JDate($packtime);
            $external[$name]['releases'][$release]['time']  = $packtime->format($jdate,true);
          }
        }
      }
      
      $path = JPluginHelper::getLayoutPath('content', 'pmjsatis', 'default');
      ob_start();
      include $path;
      $content = ob_get_clean();
      $article->text  = str_replace($matches[0],$content,$article->text);
    }
    
		return;
	}
}
?>