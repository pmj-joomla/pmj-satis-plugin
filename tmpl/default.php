<div class="row mt-2">
  <div class="col">
    <div class="d-flex justify-content-between align-items-center">
      <a href="<?php echo $config['homepage']; ?>">
        <h2><?php echo $config['name']; ?></h2>
      </a>
      <span class="badge badge-light m-1" title="<?php echo $time->format($jdate,true); ?>">
        <?php echo JText::_("PLG_CONTENT_PMJSATIS_LAST_UPDATE");?> <br class="d-md-none">
        <time class="timeago" datetime="<?php echo $time_iso; ?>"><?php echo $time->format($jdate,true); ?></time>
      </span>
    </div>
    <p class="lead"><?php echo $config['description']; ?></p>
    
    <div id="repo-help" class="my-3">
        <p class="small"><?php echo JText::_("PLG_CONTENT_PMJSATIS_PRIVATE_REPO_DESCRIPTION");?></p>

        <div id="repo-config" class="my-3 card card-outline collapse">
            <div class="card-body">
                <h5 class="card-title"><?php echo JText::_("PLG_CONTENT_PMJSATIS_PRIVATE_REPO_HOWTO_TITLE");?></h5>
                <p><?php echo JText::_("PLG_CONTENT_PMJSATIS_PRIVATE_REPO_HOWTO");?></p>
<pre class="my-3"><code>{
  "repositories": [{
    "type": "composer",
    "url": "<?php echo $config['homepage']; ?>"
  }]
}</code></pre>
            <p class="small mb-0"><?php echo JText::_("PLG_CONTENT_PMJSATIS_PRIVATE_REPO_HOWTO_INFO");?></p>
            </div>
        </div>
    </div>
    
    <?php foreach ($external as $name => $package) : ?>
    <div class="card my-2">
      <div class="card-header bg-primary">
        <div><?php echo $name; ?></div>
        <div class="ml-auto">
          <?php echo $package['version']; ?>
          <span class="badge badge-light m-0" title="<?php echo $package['time']; ?>">
            [<time class="timeago" datetime="<?php echo $package['time_iso']; ?>"><?php echo $package['time']; ?></time>]
          </span>
        </div>
      </div>
      <div class="card-body">
        <p><?php echo $package['description']; ?></p>
        <dl class="row mb-0">
          <dt class="col-sm-3 col-md-2"></dt>
          <dd class="col-sm-9 col-md-10"></dd>
          
          <?php if ($package['keywords']) : ?>
          <dt class="col-sm-3 col-md-2">Keywords</dt>
          <dd class="col-sm-9 col-md-10">
          <?php foreach ($package['keywords'] as $keyword) : ?>
            <span class="badge badge-secondary"><?php echo $keyword; ?></span>
          <?php endforeach; ?>
          </dd>
          <?php endif; ?>
          <?php if ($package['homepage']) : ?>
          <dt class="col-sm-3 col-md-2">Homepage</dt>
          <dd class="col-sm-9 col-md-10">
            <a href="<?php echo $package['homepage']; ?>" target="_blank" rel="external noopener"><?php echo $package['homepage']; ?></a>
          </dd>
          <?php endif; ?>
          <?php if ($package['license']) : ?>
          <dt class="col-sm-3 col-md-2">License</dt>
          <dd class="col-sm-9 col-md-10">
          <?php echo implode(', ',$package['license']); ?>
          </dd>
          <?php endif; ?>
          <?php if ($package['authors']) : ?>
          <dt class="col-sm-3 col-md-2">Authors</dt>
          <dd class="col-sm-9 col-md-10">
          <?php foreach ($package['authors'] as $author) : ?>
            <dl class="mb-0">
              <dt>
                <?php echo $author['homepage'] ? '<a href="'.$author['homepage'].'" target="_blank" rel="external noopener">'.$author['name'].'</a>' : $author['name']; ?>
                <?php echo $author['email'] ? ' <small>&lt;'.$author['email'].'&gt;</small>' : ''; ?>
              </dt>
              <dd><small><?php echo $author['role']; ?></small></dd>
            </dl>
          <?php endforeach; ?>
          </dd>
          <?php endif; ?>
          <dt class="col-sm-3 col-md-2">Releases</dt>
          <dd class="col-sm-9 col-md-10">
          <?php $i  = 1; ?>
          <?php foreach ($package['releases'] as $release => $data) : ?>
            <?php if (!empty($data['dist'])) : ?>
            <a href="<?php echo $data['dist']['url']; ?>" title="dist-reference: <?php echo $data['dist']['reference']; ?>" target="_blank" rel="external noopener"><?php echo $release; ?></a><?php echo $i != count($package['releases']) ? ', ' : ''; ?>
            <?php elseif ($data['source'] && strpos($data['source']['url'],'@') === false) : ?>
            <a href="<?php echo $data['source']['url']; ?>" title="source-reference: <?php echo $data['source']['reference']; ?>" target="_blank" rel="external noopener"><?php echo $release; ?></a><?php echo $i != count($package['releases']) ? ', ' : ''; ?>
            <?php else: ?>
            <span class="text-primary" title="source-reference: <?php echo $data['source']['reference']; ?>"><?php echo $release; ?></span><?php echo $i != count($package['releases']) ? ', ' : ''; ?>
            <?php endif; ?>
            
            <?php $i++; ?>
          <?php endforeach; ?>
          </dd>
        </dl>
      </div>
    </div>
    <?php endforeach; ?>
    
    <p class="text-center"><small><?php echo JText::sprintf("PLG_CONTENT_PMJSATIS_SATIS_COPYRIGHT",$version); ?></small></p>
  </div>
</div>
<pre><?php //print_r($config); ?></pre>
<pre><?php //print_r($repository); ?></pre>
<pre><?php //print_r($external); ?></pre>
<?php if (strpos($language->getTag(),'de') !== false) : ?>
<script type="text/javascript" src="/plugins/content/pmjsatis/js/jquery.timeago.de.js"></script>
<?php else : ?>
<script type="text/javascript" src="/plugins/content/pmjsatis/js/jquery.timeago.js"></script>
<?php endif; ?>

<script type="text/javascript">
jQuery(document).ready(function() {
  jQuery("time.timeago").timeago();
});
</script>